package com.example.banking.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {
    private String name;
    private String userName;
    private double currentBalance;

    public UserDTO(String name, String userName, double currentBalance) {
        this.name = name;
        this.userName = userName;
        this.currentBalance = currentBalance;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "name='" + name + '\'' +
                ", userName='" + userName + '\'' +
                ", currentBalance=" + currentBalance +
                '}';
    }


}
