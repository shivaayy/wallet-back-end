package com.example.banking.controllers;

import com.example.banking.entity.UserDTO;
import com.example.banking.model.User;
import com.example.banking.services.UserService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.banking.repositories.UserRepository;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @PostMapping(value = "/register")
    public ResponseEntity<?> registerUser(@RequestBody User user) {
        try {
            if(userService.saveUser(user)){
                return new ResponseEntity<>("new user added", HttpStatus.OK);
            }
            return new ResponseEntity<>("user already exist", HttpStatus.NOT_ACCEPTABLE);

        } catch (Exception e) {
            return new ResponseEntity<>("user registration unsuccessful", HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }


    @PutMapping(value = "/recharge")
    public ResponseEntity<?> rechargeWallet(@RequestBody JsonNode node) {
        try {
            boolean status = userService.addWalletMoney(node);
            if (status) {
                return new ResponseEntity<>("recharge successful", HttpStatus.OK);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("recharge unsuccessful", HttpStatus.INTERNAL_SERVER_ERROR);


    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> loginUser(@RequestBody JsonNode node) {
        try {

            UserDTO userDTO = userService.login(node);
            if (userDTO == null) {
                return new ResponseEntity<>("Wrong username or password", HttpStatus.FORBIDDEN);
            }
            return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    @GetMapping(value = "/balance/{id}")
    public ResponseEntity<?> getBalance(@PathVariable("id") String id){
        try {
            double balance=userService.balance(id);
            return new ResponseEntity<>(balance,HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
