package com.example.banking.controllers;

import com.example.banking.model.Transaction;
import com.example.banking.services.TransactionService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:3000")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping(value = "/transfer")
    public ResponseEntity<?> transfer(@RequestBody JsonNode node){

        boolean transferStatus= transactionService.transferAmount(node);
        if(transferStatus){
            return new ResponseEntity<>("money transfer successful", HttpStatus.OK);
        }
        return new ResponseEntity<>("insufficient balance ",HttpStatus.INTERNAL_SERVER_ERROR);

    }


    @GetMapping(value = "/statement/{id}")
    public ResponseEntity<?> statement(@PathVariable("id") String id){
        try {
            List<Transaction> statement=transactionService.getStatement(id);
            return new ResponseEntity<List<Transaction>>(statement,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
