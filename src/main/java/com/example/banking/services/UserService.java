package com.example.banking.services;

import com.example.banking.entity.UserDTO;
import com.example.banking.model.User;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    public boolean saveUser(User user);

    public boolean addWalletMoney(JsonNode node);

    public UserDTO login(JsonNode node);

    public double balance(String id);
}
