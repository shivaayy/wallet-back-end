package com.example.banking.services;

import com.example.banking.model.Transaction;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public interface TransactionService {

    public void addTransaction(Transaction transaction);

    public boolean transferAmount(JsonNode node);

    List<Transaction> getStatement(String id);

}
