package com.example.banking.services;

import com.example.banking.entity.UserDTO;
import com.example.banking.model.Transaction;
import com.example.banking.model.User;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.banking.repositories.UserRepository;

import java.util.Date;
import java.util.Optional;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TransactionService transactionService;

    @Override
    public boolean saveUser(User user) {
        Optional<User> userOptional = userRepository.findById(user.getUserName());
        if (userOptional.isPresent()) {
            return false;
        }
        user.setBalance(0.00);
        user.setActiveTransaction(false);
        userRepository.save(user);
        return true;
    }

    @Override
    public boolean addWalletMoney(JsonNode node) {
        Optional<User> userOptional = userRepository.findById(node.get("userName").asText());
        if (userOptional.isPresent()) {

            User user = userOptional.get();
            Date currentTime = new Date(System.currentTimeMillis());

            Transaction transaction = new Transaction(currentTime, user.getUserName() + currentTime.toString(),
                    user.getUserName(), "credit", node.get("creditAmount").asDouble(), "wallet recharge",
                    user.getBalance() + node.get("creditAmount").asDouble());
            transactionService.addTransaction(transaction);
            user.setBalance(user.getBalance() + node.get("creditAmount").asDouble());
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public UserDTO login(JsonNode node) {
        String userName = node.get("userName").asText();
        String password = node.get("password").asText();
        UserDTO userDTO;
        Optional<User> userOptional = userRepository.findById(userName);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (password.equals(user.getPassword())) {
                userDTO = new UserDTO(user.getName(), user.getUserName(), user.getBalance());
                return userDTO;
            }
        }
        return null;
    }

    @Override
    public double balance(String id) {
        Optional<User> userOptional = userRepository.findById(id);
        User user = userOptional.get();

        return user.getBalance();
    }
}
