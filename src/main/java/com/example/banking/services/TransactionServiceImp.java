package com.example.banking.services;
import com.example.banking.model.Transaction;
import com.example.banking.model.User;
import com.example.banking.repositories.TransactionRepository;
import com.example.banking.repositories.UserRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;

import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImp implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public void addTransaction(Transaction transaction){
        transactionRepository.save(transaction);
    }


    @Override
    public boolean transferAmount(JsonNode node){

        String fromUser=node.get("fromUser").asText();
        String toUser=node.get("toUser").asText();
        Double amount=node.get("amount").asDouble();
        String comment=node.get("comment").asText();

        Optional<User> fromUserOptional=userRepository.findById(fromUser);
        Optional<User> toUserOptional=userRepository.findById(toUser);

        if(fromUserOptional.isPresent()&&toUserOptional.isPresent()&&fromUserOptional.get().getBalance()>=amount){
            Date currentTime=new Date(System.currentTimeMillis());

            User fromUserDTO=fromUserOptional.get();
            User toUserDTO=toUserOptional.get();

            Double fromBalance=fromUserDTO.getBalance()-amount;
            Double toBalance=toUserDTO.getBalance()+amount;

            fromUserDTO.setBalance(fromBalance);
            toUserDTO.setBalance(toBalance);

            userRepository.save(fromUserDTO);
            userRepository.save(toUserDTO);

            String senderDescription="amount transfer to "+toUser;
            String receiverDescription="amount transfer from "+fromUser;

            if(!comment.equals("")){
               senderDescription=senderDescription+" ("+comment+")";
               receiverDescription=receiverDescription+" ("+comment+")";
            }
            Transaction fromTransaction=new Transaction(currentTime,fromUser+currentTime,fromUser,
                    "debit",amount,senderDescription,fromBalance);


            Transaction toTransaction=new Transaction(currentTime,fromUser+currentTime,toUser,
                    "credit",amount,receiverDescription,toBalance);

            transactionRepository.save(fromTransaction);
            transactionRepository.save(toTransaction);

            return true;
        }
        return false;


    }
    @Override
    public List<Transaction> getStatement(String id){

        String userName=id;
        List<Transaction> statements=transactionRepository.findByUserName(userName);
        System.out.println(statements);
        return statements;


    }

}
