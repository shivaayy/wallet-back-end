package com.example.banking.model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@Document(collection = "transactions")
public class Transaction {

    @Id
    private String transactionId;
    private Date transactionTime;
    private String referenceNo;
    private String userName;
    private String transactionType;
    private double transactionAmount;
    private String description;
    private double balance;

    public Transaction(Date transactionTime, String referenceNo, String userName, String transactionType,
                       double transactionAmount, String description, double balance) {
        this.transactionTime = transactionTime;
        this.referenceNo = referenceNo;
        this.userName = userName;
        this.transactionType = transactionType;
        this.transactionAmount = transactionAmount;
        this.description = description;
        this.balance = balance;
    }


    @Override
    public String toString() {
        return "Transaction{" +
                "transactionId='" + transactionId + '\'' +
                ", transactionTime=" + transactionTime +
                ", referenceNo='" + referenceNo + '\'' +
                ", userName='" + userName + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", description='" + description + '\'' +
                ", balance=" + balance +
                '}';
    }


}
