package com.example.banking.repositories;

import com.example.banking.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction,String> {

    @Query("{'userName':?0}")
    List<Transaction> findByUserName(String userName);

}
