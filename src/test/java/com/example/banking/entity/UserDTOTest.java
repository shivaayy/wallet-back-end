package com.example.banking.entity;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDTOTest {

    @Test
    @DisplayName("Testing userDTO setter methods")
    void userDtoSetterMethodsTesting(){
        UserDTO userDTO=new UserDTO();

        String name="Shivam Yadav";
        String userName="shivam.yadav@nextuple.com";
        double currentBalance=100.0;
        userDTO.setUserName(userName);
        userDTO.setName(name);
        userDTO.setCurrentBalance(currentBalance);

        assertEquals(name,userDTO.getName());
        assertEquals(userName,userDTO.getUserName());
        assertEquals(currentBalance,userDTO.getCurrentBalance());
    }

}