package com.example.banking.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionTest {
    @Test
    @DisplayName("Testing transaction class argument constructor")
    void transactionClassArgsConstructorTest(){
        String transactionId=null;
        Date transactionTime=new Date();
        String referenceNo="nssjdjdijwodko";
        String userName="shivam.yadav@nextuple.com";
        String transactionType="credit";
        double transactionAmount=100.0;
        String description="wallet recharge";
        double balance=1000000.0;
        String toString="Transaction{" +
                "transactionId='" + transactionId + '\'' +
                ", transactionTime=" + transactionTime +
                ", referenceNo='" + referenceNo + '\'' +
                ", userName='" + userName + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", description='" + description + '\'' +
                ", balance=" + balance +
                '}';

        Transaction transaction=new Transaction(transactionTime,referenceNo,userName,transactionType,transactionAmount,
                description,balance);
        assertEquals(transactionId,transaction.getTransactionId());
        assertEquals(transactionTime,transaction.getTransactionTime());
        assertEquals(referenceNo,transaction.getReferenceNo());
        assertEquals(userName,transaction.getUserName());
        assertEquals(transactionType,transaction.getTransactionType());
        assertEquals(transactionAmount,transaction.getTransactionAmount());
        assertEquals(description, transaction.getDescription());
        assertEquals(balance,transaction.getBalance());
        assertEquals(toString,transaction.toString());


    }

    @Test
    @DisplayName("Testing transaction class no argument constructor")
    void transactionClassNoArgsConstructorTest(){

        Transaction transaction=new Transaction();
        assertEquals(null,transaction.getTransactionId());
        assertEquals(null,transaction.getTransactionTime());
        assertEquals(null,transaction.getTransactionType());
        assertEquals(0.0,transaction.getTransactionAmount());
        assertEquals(null,transaction.getUserName());
        assertEquals(null,transaction.getReferenceNo());
        assertEquals(0.0,transaction.getBalance());
        assertEquals(null,transaction.getDescription());


    }
    @Test
    @DisplayName("Testing transaction class setter methods")
    void transactionClassSettertest(){
        Transaction transaction=new Transaction();
        String transactionId=null;
        Date transactionTime=new Date();
        String referenceNo="nssjdjdijwodko";
        String userName="shivam.yadav@nextuple.com";
        String transactionType="credit";
        double transactionAmount=100.0;
        String description="wallet recharge";
        double balance=1000000.0;
        transaction.setTransactionId(transactionId);
        transaction.setTransactionTime(transactionTime);
        transaction.setReferenceNo(referenceNo);
        transaction.setTransactionType(transactionType);
        transaction.setTransactionAmount(transactionAmount);
        transaction.setUserName(userName);
        transaction.setBalance(balance);
        transaction.setDescription(description);

        assertEquals(transactionId,transaction.getTransactionId());
        assertEquals(transactionTime,transaction.getTransactionTime());
        assertEquals(referenceNo,transaction.getReferenceNo());
        assertEquals(userName,transaction.getUserName());
        assertEquals(transactionType,transaction.getTransactionType());
        assertEquals(transactionAmount,transaction.getTransactionAmount());
        assertEquals(description, transaction.getDescription());
        assertEquals(balance,transaction.getBalance());



    }
}
