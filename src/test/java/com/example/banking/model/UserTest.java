package com.example.banking.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UserTest {
    @Test
    @DisplayName("testing user class all argument constructor")
    void userClassAllArgsConstructorTest(){
        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double balance=1000.0;
        boolean activeTransaction=false;

        String toString="User{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", activeTransaction=" + activeTransaction +
                '}';
        User user=new User(userName,password,name,balance,activeTransaction);
        assertEquals(userName,user.getUserName());
        assertEquals(password,user.getPassword());
        assertEquals(name,user.getName());
        assertEquals(balance,user.getBalance());
        assertEquals(activeTransaction,user.isActiveTransaction());

        assertEquals(toString,user.toString());

    }

    @Test
    @DisplayName("testing user class no argument constructor")
    void userClassNoArgsConstructorTest(){
        User user=new User();
        assertEquals(null,user.getUserName());
        assertEquals(null,user.getPassword());
        assertEquals(null,user.getName());
        assertEquals(0.0,user.getBalance());
        assertEquals(false,user.isActiveTransaction());


    }
}
