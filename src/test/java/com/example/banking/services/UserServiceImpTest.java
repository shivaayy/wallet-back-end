package com.example.banking.services;

import com.example.banking.entity.UserDTO;
import com.example.banking.model.User;
import com.example.banking.repositories.TransactionRepository;
import com.example.banking.repositories.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
@SpringBootTest
class UserServiceImpTest {

    @Autowired
    private UserServiceImp userServiceImp;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private TransactionServiceImp transactionServiceImp;

    @MockBean
    private TransactionRepository transactionRepository;

    @Captor
    private ArgumentCaptor<User> userArgumentCaptor;

    @Test
    @DisplayName("user already exist in user collection")
    void userAlreadyExist() {
        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double balance=0.0;
        User user=new User(userName,password,name,balance,false);
        Mockito.when(userRepository.findById(userName)).thenReturn(Optional.of(user));
        assertEquals(false,userServiceImp.saveUser(user));

    }

    @Test
    @DisplayName("saving new user into database")
    void saveNewUserIntoUserCollection() {
        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double balance=0.0;
        User user=new User(userName,password,name,balance,false);
        userServiceImp.saveUser(user);
        Mockito.verify(userRepository,Mockito.times(1)).save(userArgumentCaptor.capture());
        assertEquals(userName,userArgumentCaptor.getValue().getUserName());
        assertEquals(password,userArgumentCaptor.getValue().getPassword());
        assertEquals(name,userArgumentCaptor.getValue().getName());
        assertEquals(balance,userArgumentCaptor.getValue().getBalance());

    }

    @Test
    @DisplayName("failed to recharge user account")
    void failToAddMoney() throws JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"userName\":\"shivam.yadav@nextuple.com\",\"creditAmount\":\"100.0\"}";
        JsonNode jsonNode=objectMapper.readTree(json);
        assertEquals(false,userServiceImp.addWalletMoney(jsonNode));
    }

    @Test
    @DisplayName("successful wallet recharge")
    void rechargeMoneyAddedtoUserAccount() throws JsonProcessingException {

        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"userName\":\"shivam.yadav@nextuple.com\",\"creditAmount\":\"100.0\"}";
        JsonNode jsonNode=objectMapper.readTree(json);

        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double balance=0.0;
        User user=new User(userName,password,name,balance,false);
        Mockito.when(userRepository.findById(userName)).thenReturn(Optional.of(user));

        assertEquals(true,userServiceImp.addWalletMoney(jsonNode));

    }

    @Test
    @DisplayName("checking credential for login")
    void loginPass() throws JsonProcessingException {
        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double currentBalance=1000.0;
        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"userName\":\"shivam.yadav@nextuple.com\",\"password\":\"xyz\"}";
        JsonNode jsonNode=objectMapper.readTree(json);

        User user=new User(userName,password,name,currentBalance,false);

        UserDTO userDTO=new UserDTO(name,userName,currentBalance);
        Mockito.when(userRepository.findById(userName)).thenReturn(Optional.of(user));

        assertEquals(userDTO.toString(),userServiceImp.login(jsonNode).toString());


    }

    @Test
    @DisplayName("checking login credential for incorrect password")
    void loginFail() throws JsonProcessingException {
        String userName="shivam.yadav@nextuple.com";
        String password="xyz";
        String name="Shivam Yadav";
        double currentBalance=1000.0;
        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"userName\":\"shivam.yadav@nextuple.com\",\"password\":\"xyzShivam\"}";
        JsonNode jsonNode=objectMapper.readTree(json);

        User user=new User(userName,password,name,currentBalance,false);

        UserDTO userDTO=new UserDTO(name,userName,currentBalance);
        Mockito.when(userRepository.findById(userName)).thenReturn(Optional.of(user));

        assertEquals(null,userServiceImp.login(jsonNode));


    }

    @Test
    @DisplayName("get user current balance")
    void getUserBalance() {
        String userName="shivam.yadav@nextuple.com";
        double balance=1000.0;
        User user1=new User("shivam.yadav@nextuple.com","xyz","Shivam Yadav",balance,false);

        Mockito.when(userRepository.findById(userName)).thenReturn(java.util.Optional.of(user1));

        assertEquals(balance,userServiceImp.balance(userName));


    }
}