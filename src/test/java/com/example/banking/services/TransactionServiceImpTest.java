package com.example.banking.services;

import com.example.banking.model.Transaction;
import com.example.banking.model.User;
import com.example.banking.repositories.TransactionRepository;

import com.example.banking.repositories.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class TransactionServiceImpTest {

    @Autowired
    private TransactionServiceImp transactionServiceImp;

    @MockBean
    private TransactionRepository transactionRepository;

    @MockBean
    private UserRepository userRepository;


    @Test
    @DisplayName("transfer amount from user A to B")
    void transferAmount() throws JsonProcessingException {
        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"fromUser\":\"shivam.yadav@nextuple.com\",\"toUser\":\"abhi@nextuple.com\",\"amount\":\"100.0\",\"comment\":\"gift\"}";
        JsonNode jsonNode=objectMapper.readTree(json);
        User fromUser=new User("shivam.yadav@nextuple.com",null,null,10000.0,false);
        User toUser=new User("abhi@nextuple.com",null,null,1000.0,false);
        Mockito.when(userRepository.findById("shivam.yadav@nextuple.com")).thenReturn(Optional.of(fromUser));
        Mockito.when(userRepository.findById("abhi@nextuple.com")).thenReturn(Optional.of(toUser));

        assertEquals(true,transactionServiceImp.transferAmount(jsonNode));
    }


    @Test
    @DisplayName("transfer fail due to insufficient balance")
    void transferAmountFailed() throws JsonProcessingException {
        ObjectMapper objectMapper=new ObjectMapper();
        String json="{\"fromUser\":\"shivam.yadav@nextuple.com\",\"toUser\":\"abhi@nextuple.com\",\"amount\":\"100.0\",\"comment\":\"gift\"}";
        JsonNode jsonNode=objectMapper.readTree(json);
        User fromUser=new User("shivam.yadav@nextuple.com",null,null,10.0,false);
        User toUser=new User("abhi@nextuple.com",null,null,1000.0,false);
        Mockito.when(userRepository.findById("shivam.yadav@nextuple.com")).thenReturn(Optional.of(fromUser));
        Mockito.when(userRepository.findById("abhi@nextuple.com")).thenReturn(Optional.of(toUser));

        assertEquals(false,transactionServiceImp.transferAmount(jsonNode));
    }

    @Test
    @DisplayName("get user transactions")
    void getStatement() {

        String userName="shivam.yadav@nextuple.com";
        Date date=new Date();
        Transaction transaction1= new Transaction(date,"abhi@nextuple.comThu Jul 15 15:49:08 IST 2021",
                userName,"credit",1000.0,"wallet recharge",
                1000.0);
        Transaction transaction2= new Transaction(date,"abhi@nextuple.comThu Jul 15 15:49:08 IST 2021",
                userName,"debit",100.0,"amount transfer to abhi@nextuple.com",
                900.0);

        List<Transaction> transactions=Stream.of(transaction1,transaction2).collect(Collectors.toList());


        Mockito.when(transactionRepository.findByUserName(userName))
                .thenReturn(transactions);

        assertEquals(transactions,transactionServiceImp.getStatement(userName));


    }
}